#! /usr/bin/env ruby
require 'yaml'
require 'csv'
require 'json'
require 'erb'

class Client
  attr_reader :issue_title
  def initialize(issue_title, private_api_key = nil)
    @issue_title = issue_title
    @private_key = private_api_key
  end
  def get(args)
    `curl -s --header 'private-token: #{@private_key}' '#{args}'`
  end
  def put(args)
    `curl --request PUT -s --header 'private-token: #{@private_key}' '#{args}'`
  end

  def releases
    @releases ||= get "https://gitlab.com/api/v4/projects/278964/releases"
  end

  def issue
    @issue ||=(
      search = ERB::Util.url_encode(issue_title)
      get "https://gitlab.com/api/v4/search?scope=issues&search=#{search}"
    )
  end

  def issue_id
    JSON(issue)[0]['iid']
  end

  def update_issue(description)
    desc = ERB::Util.url_encode(description)
    url = "https://gitlab.com/api/v4/projects/7764/issues/#{issue_id}?description=#{desc}"
    put url
  end

end

class RDTaxCredit
  attr_reader :year, :client
  def issue_title
    "FY#{year} R&D Tax Credit - Supporting Documention"
  end

  def initialize(year)
    @year = year
    warn "running for FY#{year}"
    @client = Client.new(issue_title, ENV['GITLAB_PRIVATE_TOKEN'])
    process_release_posts
  end


  def fiscal_year
    @fiscal_year ||= Date.parse("#{year - 1}-02-01")..Date.parse("#{year.to_i}-01-31")
  end

  def releases_api_response
    @releases_api_response ||= JSON(client.releases)
  end

  def releases
    releases_api_response.reject do |item|
      not fiscal_year === Date.parse(item['released_at'])
    end
  end

  def releases_summary
    releases.inject({}) do |memo, item|
      version = item['name'].gsub(/[^0-9.]/, '')
      memo[version] = {
        version: version,
        url: "https://gitlab.com/gitlab-org/gitlab/-/releases/v#{version}.0-ee",
        released_at: Date.parse(item['released_at']).strftime("%Y-%m-%d"),
        release_post_url: item['assets']['links'][0]['url'],
        total_features: @counts[version][:features],
        top_features: @counts[version][:top],
        primary_features: @counts[version][:primary],
        secondary_features: @counts[version][:secondary],
      }
      memo
    end
  end

  def features_by_version
    @features
  end

  def total_features
    releases_summary.inject(0) do |memo, (version, item)|
      memo + item[:total_features]
    end
  end


  def release_numbers
    releases.map do |item|
      item['name'].gsub(/[^0-9.]/, '')
    end.sort_by {|v| Gem::Version.new(v)}
  end

  def release_post_urls
    releases.map do |item|
      memo.push [item['assets']['links'][0]['name'],item['assets']['links'][0]['url']]
    end
  end

  def csv
    @csv.inject("") do |memo, line|
      memo << line.to_csv
    end
  end

  def counts
    @counts.keys.sort.each do |key|
      puts "#{key}, #{@counts[key]}"
    end
    puts "total, #{@counts.values.inject(&:+)}"
  end

  def issue
    client.issue || raise("Can't find issue")
  end
  def issue_description_header
    <<-HERE
## Context

Companies are eligible to receive a United States R&D Tax Credit.  This issue provides supporting documentation for GitLab's FY#{year} R&D Tax Credit calculation.  Activities and expenses that qualify for this deduction generally must be performed in the US and meet the 4 part test summarized below.

### "4 Part" Qualified Research Test

To be considered R&D for the purpose of this credit:

- Activities must be related to the development or improvement of a product, process, computer software, technique or invention
- Activities much meet these 4 criteria
    1. Did technological uncertainty exist?
        - Activities must intend to discover information that would eliminate uncertainty concerning the development or improvement of a product.
    1. Was there a process of experimentation?
        - Process designed to evaluate one or more alternatives to achieve a result where the capability or the method of achieving that result, or the appropriate design of that result, is uncertain as of the beginning of the activity. 
    1. Is it technological in nature?
        - Activity relies on principles of hard science, engineering, or computer science
    1. Does the research have a qualified purpose?
        - Purpose of activity related to a new or improved function, performance, reliability, or quality of the business component

## GitLab’s Research & Development Process

GitLab has defined a standard process for R&D which is linked below and which meets the 4 part test.  Features described in this issue follow this standard company R&D process.

https://about.gitlab.com/handbook/product-development-flow/


   HERE
  end

  def issue_description_footer
    <<-HERE

### Qualified Expenses

* TBA - List qualifying contracts and expenses here


## Methodology

GitLab's methodology for calculating this credit can be found here in the GitLab handbook (Link TBA)

## Tooling

This issue was generate using code in [https://gitlab.com/gitlab-org/ci-cd/r-and-d-tax-credit-automation](https://gitlab.com/gitlab-org/ci-cd/r-and-d-tax-credit-automation)

## Actions

* [ ] Add contracts that qualify for tax credit
* [ ] Review infrastructure costs to determine if any meet the 4 part test
* [ ] TBA
   HERE
  end

  def issue_description
    d = issue_description_header
    d << "## Summary of R&D Effort in FY#{year}\n"
    d << <<-MARKDOWN
### In-House R&D Projects

During FY#{year} GitLab's R&D team released #{release_numbers.size} new versions of the software (#{release_numbers[0]} - #{release_numbers[-1]}) containing a total of #{total_features} new features.  These features were all developed using the R&D development process linked to above.  The table below shows the break down of new features by version:
    MARKDOWN
    d << "| Version | Release Date | New Features Added | Customer Facing Release Post |\n"
    d << "| ------ | ------ | ------ | ------ |\n"
    release_numbers.each do |version|
      r = releases_summary[version]
      d << <<-MARKDOWN
|#{version}|#{r[:released_at]}|[#{r[:total_features]}](#{r[:url]})|#{r[:release_post_url]}|
      MARKDOWN
    end
    d << issue_description_footer
  end
  # version: version,
  # url: "https://gitlab.com/gitlab-org/gitlab/-/releases/v#{version}.0-ee",
  # released_at: Date.parse(item['released_at']).strftime("%Y-%m-%d"),
  # release_post_url: item['assets']['links'][0]['url'],
  # total_features: @counts[version][:features],
  # top_features: @counts[version][:top],
  # primary_features: @counts[version][:primary],
  # secondary_features: @counts[version][:secondary],
  def update_issue_description
    client.update_issue(issue_description)
  end

  def release_post_files
    clone_prefix = '/Users/samgoldstein/dev'
    fns = Dir["#{clone_prefix}/www-gitlab-com/data/release_posts/*/*.yml"]
  end

  def process_release_posts
    @counts = {}
    @features = {}
    @csv = [["Version", "Name", "Stage", "R&D Issue URL", "Customer Facing Documentation URL", "Label", "Descriptions"]]
    release_post_files.each do |fn|
      version = fn.split('/')[-2].sub('_', '.')
      next unless release_numbers.include?(version)
      if !@counts[version]
        @counts[version] = {features: 0, top: 0, primary: 0, secondary: 0}
      end
      if !@features[version]
        @features[version] = []
      end
      yaml = File.read(fn)
      data = YAML.load(yaml)
      if data['features']
        %w| top primary secondary |.each do |type|
          ff = data['features'][type]
          if ff
            @counts[version][:features] += ff.size
            @counts[version][type.intern] += ff.size
            ff.each do |f|
              @features[version].push(["v"+version,f['name'], f['stage'], f['issue_url']||f['epic_url'], f['documentation_link'], type, f['description']] )
              @csv.push(["v"+version,f['name'], f['stage'], f['issue_url']||f['epic_url'], f['documentation_link'], type] )
            end
          end
        end
      end
    end
  end
end


year = ARGV[0] || Date.today.year 
report = RDTaxCredit.new(year.to_i)
pp report.update_issue_description
